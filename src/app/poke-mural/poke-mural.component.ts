import { environment } from './../../environments/environment.prod';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-poke-mural',
  templateUrl: './poke-mural.component.html',
  styleUrls: ['./poke-mural.component.scss']
})
export class PokeMuralComponent implements OnInit {
  pokeCounter: number;    // There are 892 pokemons in 09/2020
  pokedexUrl: string;     // base url for the pokemons API
  pokeImgUrls: string[];  // stores the all the img urls
  pokeNames: string[];    // list of pokemon names
  pokemon;                // access to all data of an specific pokemon
  imgUrl: string;
  id: string;

  constructor() {
    this.pokeImgUrls = [];
    this.pokeNames = [];
    this.pokeCounter = 892;

    this.setImgUrls();
    this.fetchPokemons();
  }

  ngOnInit(): void{ }

  /* This function will get a specific img url based on the pokemon id */
  getImgUrl(id: number): string{
    const str = (id + 1).toString().padStart(3, '0'); // 3 digit number
    const url = 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/' + str + '.png';
    return url;
  }

  /* This function sets all img urls to pokeImgUrls (: string[]) */
  setImgUrls(): void{
    for (let i = 0; i < this.pokeCounter; i++){
      this.pokeImgUrls[i] = this.getImgUrl(i);
    }
  }

  // This function gets the response of ONE pokemon data, storing it's name
  getPokemon = async (id) => {
    const url = environment.apiURL + id;
    const res = await fetch(url);

    this.pokemon = await res.json();
    this.pokeNames[id - 1] = this.pokemon.name;
  }

  // This function stores all the pokemon names
  fetchPokemons = async () => {
    for (let i = 1; i <= this.pokeCounter; i++){
      await this.getPokemon(i);
    }
  }

}
