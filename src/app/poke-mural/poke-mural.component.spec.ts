import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeMuralComponent } from './poke-mural.component';

describe('PokeMuralComponent', () => {
  let component: PokeMuralComponent;
  let fixture: ComponentFixture<PokeMuralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokeMuralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeMuralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
