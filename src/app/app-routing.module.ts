import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { PokeDetailsComponent } from './poke-details/poke-details.component';
import { PokeMuralComponent } from './poke-mural/poke-mural.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './shared/components/header/header.component';

const routes: Routes = [
  {
    path: '',
    component: PokeMuralComponent,
  },
  {
    path: 'details/:id',
    component: PokeDetailsComponent
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
