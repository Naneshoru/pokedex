import { environment } from './../../environments/environment.prod';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-poke-details',
  templateUrl: './poke-details.component.html',
  styleUrls: ['./poke-details.component.scss']
})
export class PokeDetailsComponent implements OnInit {

  name;
  mainTypes;  // pokemon types: [fire, grass, eletric...]
  pokeTypes;  // pokemon types (in the api)
  type;       // the main type of a specific pokemon
  color;      // the selected color
  abilities;  // list of abilities (in the api)
  moves;      // list of moves (in the api)
  versions;   // list of versions of a pokemon (in the api)
  imgUrl;     // the image url of a pokemon (external)

  pokemon;    // access to all data of an specific pokemon

  colors = {
    fire: '#FDDFDF',
    grass: '#DEFDE0',
    electric: '#FCF7DE',
    water: '#DEF3FD',
    ground: '#f4e7da',
    rock: '#d5d5d4',
    fairy: '#fceaff',
    poison: '#98d7a5',
    bug: '#f8d5a3',
    dragon: '#97b3e6',
    psychic: '#eaeda1',
    flying: '#F5F5F5',
    fighting: '#E6E0D4',
    normal: '#F5F5F5'
  };

  subscription: Subscription;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void{
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        this.pokemon = this.getPokemon(params.id);
      }
    );
   }

 // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  // Gets all the pokemon details
  getPokemon = async (id: any) => {
    const url = environment.apiURL + id;
    const res = await fetch(url);
    this.pokemon = await res.json();

    this.mainTypes = Object.keys(this.colors);
    this.abilities = this.pokemon.abilities.map(a => a.ability.name);
    this.pokeTypes = this.pokemon.types.map(type => type.type.name);
    this.type = this.mainTypes.find(type => this.pokeTypes.indexOf(type) > -1); // The color of the background is the first type
    this.color = this.colors[this.type];
    this.moves = this.pokemon.moves.map(m => ' ' + m.move.name);
    this.versions = this.pokemon.game_indices.map(v => ' ' + v.version.name);

    this.imgUrl = 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/' +
                  this.pokemon.id.toString().padStart(3, '0') + '.png'; // 3 digit number

    document.getElementById('stats').style.backgroundColor = this.color;
    document.getElementById('more-info').style.backgroundColor = this.color + '55';
  }

}
