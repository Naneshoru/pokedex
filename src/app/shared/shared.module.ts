import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SearchService } from './services/search.service.';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { PokeMuralComponent } from './../poke-mural/poke-mural.component';
import { PokeDetailsComponent } from './../poke-details/poke-details.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    HeaderComponent,
    PokeMuralComponent,
    PokeDetailsComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    FormsModule
  ],
  providers: [
    SearchService
  ]
})
export class SharedModule { }
