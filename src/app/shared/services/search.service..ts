import { environment } from './../../../environments/environment.prod';
import { HttpClientModule, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  id;
  pokemon;
  erro: HttpErrorResponse;

  constructor(private http: HttpClient,
              private router: Router) {

  }

  getData(id): Observable<any>{
    this.erro = null;
    return this.pokemon =  this.http.get<any>(environment.apiURL + id)
      .pipe(
        tap(() => {
          this.router.navigate(['details/' + id ]);
        }),
        catchError((err) => {
          this.router.navigate(['not-found']);
          this.erro = err;
          return throwError(err);
        })
      );
  }

  getPokemon = async (id) => {
    const url = environment.apiURL + id;
    const res = await fetch(url);

    this.pokemon = await res.json();
  }
}
