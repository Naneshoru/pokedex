import { SearchService } from './../../services/search.service.';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  pokemon;

  constructor(private service: SearchService) {

  }

  ngOnInit(): void {
  }

  search(id): void{
    this.pokemon = this.service.getData(id)
      .subscribe(dados => this.pokemon = dados);
  }
}
